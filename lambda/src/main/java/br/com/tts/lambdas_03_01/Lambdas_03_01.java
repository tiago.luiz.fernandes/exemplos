package br.com.tts.lambdas_03_01;

import java.util.ArrayList;

public class Lambdas_03_01 {

    public static void main(String[] args) {
        Pessoa t = new Pessoa();
        ArrayList<String> list = new ArrayList<>();

        list.add("tiago");
        list.add("luiz");
        list.add("fernandes");
        list.add("tiago");

        System.out.println(list);

        list.stream().filter(n -> n.equals("tiago")).findFirst().ifPresent(n2 -> t.setNome(n2));

        System.out.println(t.getNome());


    }

}
