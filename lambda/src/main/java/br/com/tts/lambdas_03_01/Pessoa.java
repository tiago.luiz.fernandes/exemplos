package br.com.tts.lambdas_03_01;


public class Pessoa {
    private String nome;

    public void setNome(String nome){
        this.nome = nome;
    }

    public String getNome(){
        return this.nome;
    }

}
