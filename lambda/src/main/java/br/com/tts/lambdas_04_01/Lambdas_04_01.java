package br.com.tts.lambdas_04_01;

import lombok.val;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class Lambdas_04_01 {

    public static void main(String[] args) {
        List<Pais> america = new ArrayList<>();

        Pais brasil = new Pais();
        brasil.setCode(1);
        brasil.setName("Brasil");

        Estado rioGrandeDoSul = new Estado();
        Estado santaCatarina = new Estado();
        Estado parana = new Estado();

        Cidade curitiba = new Cidade();
        parana.setCidades(Arrays.asList(curitiba));

        brasil.setEstados(Arrays.asList(rioGrandeDoSul,santaCatarina,parana));

        Pais estadosUnidos = new Pais();
        estadosUnidos.setCode(2);
        estadosUnidos.setName("Estados Unidos");
        estadosUnidos.setEstados(null);

        america.add(brasil);
        america.add(estadosUnidos);

        america
                .stream()
                .filter(pais -> pais.getEstados() != null)
                .forEach(pais -> { pais.getEstados()
                        .stream()
                        .filter(estado -> estado.getCidades() != null)
                        .forEach(estado -> { estado.getCidades()
                                .stream()
                                .map(cidade -> pais + " " + estado + " " + cidade)
                                .forEach(System.out::println);
            });
        });

        america
                .stream()
                .filter(pais -> pais.getEstados() != null)
                .forEach(pais -> { pais.getEstados()
                        .stream()
                        .filter(estado -> { return estado.getCidades() != null; })
                        .forEach(estado -> { estado.getCidades()
                                .stream()
                                .map(cidade -> pais + " " + estado + " " + cidade)
                                .forEach(System.out::println);
                        });
                });


    }

}
