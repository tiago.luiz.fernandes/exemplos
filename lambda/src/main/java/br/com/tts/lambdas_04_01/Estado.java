package br.com.tts.lambdas_04_01;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class Estado {
    private int code;
    private String name;
    private List<Cidade> cidades;
}
