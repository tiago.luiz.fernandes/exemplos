package br.com.tts.mapstructsamples.mapper;

import br.com.tts.mapstructsamples.domain.TargetSub;
import br.com.tts.mapstructsamples.dto.SourceSub;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.IterableMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

import java.util.List;
import java.util.Set;

@Mapper
public interface SourceSubTargetSubMapper {

    SourceSubTargetSubMapper MAPPER = Mappers.getMapper( SourceSubTargetSubMapper.class );

    @Mappings({
            @Mapping(target = "id", source = "codigo"),
            @Mapping(target = "phone", source = "telefone")
    })
    TargetSub toTarget(SourceSub s );

    @InheritInverseConfiguration
    SourceSub toSource(TargetSub t );

    List<TargetSub> toTarget(List<SourceSub> sourceSubList);

    List<SourceSub> toSource(List<TargetSub> targetSubList);

}
