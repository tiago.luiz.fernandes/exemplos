package br.com.tts.mapstructsamples.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class Source {
    Long codigo;
    String primeiroNome;
    String ultimoNome;
    Boolean ativo;
    String email;

    List<SourceSub> list;

}
