package br.com.tts.mapstructsamples.mapper;

import br.com.tts.mapstructsamples.domain.Target;
import br.com.tts.mapstructsamples.dto.Source;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

@Mapper(uses = {SourceSubTargetSubMapper.class})
public interface SourceTargetMapper {

    SourceTargetMapper MAPPER = Mappers.getMapper( SourceTargetMapper.class );

    @Mappings({
            @Mapping(target = "id", source = "codigo"),
            @Mapping(target = "firstName", source = "primeiroNome"),
            @Mapping(target = "lastName", source = "ultimoNome"),
            @Mapping(target = "active", source = "ativo")
    })
    Target toTarget(Source s );

    @InheritInverseConfiguration
    Source toSource(Target t );



}