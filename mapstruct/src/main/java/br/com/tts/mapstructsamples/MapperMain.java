package br.com.tts.mapstructsamples;

import br.com.tts.mapstructsamples.domain.Target;
import br.com.tts.mapstructsamples.domain.TargetSub;
import br.com.tts.mapstructsamples.dto.Source;
import br.com.tts.mapstructsamples.dto.SourceSub;
import br.com.tts.mapstructsamples.mapper.SourceSubTargetSubMapper;
import br.com.tts.mapstructsamples.mapper.SourceTargetMapper;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class MapperMain {

    public static void main(String[] args) {
        //testToTarget();
        //testToSource();
        testList();
    }


    private static void testToTarget(){

        System.out.println("------------------");
        System.out.println("------------------");
        System.out.println("testToTarget begin");

        Source source = new Source();
        source.setCodigo(1L);
        source.setPrimeiroNome("Tiago");
        source.setUltimoNome("Fernandes");
        source.setAtivo(true);
        source.setEmail("tiago@user.com");

        List<SourceSub> sourceSubList = new ArrayList<>();

        sourceSubList.add(new SourceSub(1L,11L,"Texto 1"));
        sourceSubList.add(new SourceSub(2L,22L,"Texto 2"));

        source.setList(sourceSubList);

        System.out.println(source.toString());
        System.out.println(source.getList().toString());

        Target target = SourceTargetMapper.MAPPER.toTarget(source);
        System.out.println(target.toString());
        System.out.println(target.getList().toString());

        System.out.println("testToTarget   end");
        System.out.println("------------------");
        System.out.println("------------------");


    }

    private static void testToSource(){

        System.out.println("testToSource  begin");

        Target target = new Target();

        target.setId(2L);
        target.setFirstName("Isabela");
        target.setLastName("Costa Fernandes");
        target.setActive(false);
        target.setEmail("isabela@user.com");

        List<TargetSub> targetSubList = new ArrayList<>();

        targetSubList.add(new TargetSub(1L,11L,"Texto 1"));
        targetSubList.add(new TargetSub(2L,22L,"Texto 2"));

        target.setList(targetSubList);

        System.out.println(target.toString());
        target.getList().stream().forEach( targetSub -> System.out.println(targetSub.toString()));

        Source s = SourceTargetMapper.MAPPER.toSource(target);
        System.out.println(s.toString());
        if ( s.getList().size() > 0 ) {
            s.getList().stream().forEach( sourceSub -> sourceSub.toString());
        }


        System.out.println("testToSource   end");

    }

    private static void testList() {

        List<SourceSub> sourceSubList = new ArrayList<>();
        List<TargetSub> targetSubList = new ArrayList<>();

        sourceSubList.add(new SourceSub(1L,10L,"Texto 100"));
        sourceSubList.add(new SourceSub(2L,20L,"Texto 200"));

        //Set<SourceSub> sourceSubListSet = new HashSet<>(sourceSubList);
        //Set<TargetSub> targetSubListSet = new HashSet<>();

        targetSubList = SourceSubTargetSubMapper.MAPPER.toTarget(sourceSubList);

        //targetSubList.addAll(targetSubListSet);

        System.out.println(sourceSubList);
        System.out.println(targetSubList);

    }

}
