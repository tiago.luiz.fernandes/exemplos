package br.com.tts.mapstructsamples.domain;

import br.com.tts.mapstructsamples.dto.SourceSub;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class Target {
    Long id;
    String firstName;
    String lastName;
    Boolean active;
    String email;

    List<TargetSub> list;
}
